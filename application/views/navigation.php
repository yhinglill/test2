<div class="navbar navbar-inverse navbar-fixed-top">
  	<div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">A simple image file uploading app</a>
          <div class="nav-collapse collapse">
			<div id="thanks"><p><a href="#form-content" class="confirm-new btn-primary btn-large"  data-id="addid">Add New</a></p></div>
		  </div><!--/.nav-collapse -->
        </div>
  	</div>
</div>