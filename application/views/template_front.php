<!DOCTYPE html>
<html>
<head>
<title>A simple image file uploading app</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
    
<style>
body { margin: 50px;  }
		.well { background: #fff; text-align: center; }
		.modal { text-align: left; }
		body {
	padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		}
</style>    

</head>
<body>
	<?php if(isset($navigation)) echo $navigation; ?>
	<?php if(isset($content)) echo $content; ?>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script type='text/javascript'>
        
        $(document).ready(function() {
        
              $('#myModal').on('show', function() {
				var tit = $('.confirm-delete').data('title');

				$('#myModal .modal-body p').html("Are you sure you want to delete " + '<b>' + tit +'</b>' + ' ?');
				var id = $(this).data('id'),
				removeBtn = $(this).find('.danger');
			})

			$('.confirm-delete').on('click', function(e) {
				e.preventDefault();

				var id = $(this).data('id');
				$('#myModal').data('id', id).modal('show');
			});

			$('#btnYes').click(function() {
					var id = $('#myModal').data('id');
						$.ajax({
							type: "POST",
							url: "<?php echo URL::base('http'); ?>index.php/welcome/deletedata/"+id+"",
							success: function(){
								$('[data-id='+id+']').parents('tr').remove();
								$('#myModal').modal('hide');
								jQuery('#thanks2').text('Data has been deleted.');
							},
							error: function(){
								alert("Fail to delete, try again later.");
							}
						});
			
			});
			
			$('.confirm-edit').on('click', function(e) {
				e.preventDefault();
				var id = $(this).data('id');
				$('#form-content').data('id', id).modal('show');
			});
			$('.confirm-new').on('click', function(e) {
				e.preventDefault();
				var id = $(this).data('id');
				$('#form-content').data('id', id).modal('show');
			});
			$("input#submit").click(function(){
				var formData = new FormData();
				var id = $('#form-content').data('id');
				//alert(id);
				formData.append('file', $('input[type=file]')[0].files[0]);
				$.ajax({
					type: "POST",
					url: "<?php echo URL::base('http'); ?>index.php/welcome/uploadnew/"+id+"",
					data: formData,
					async: false,
					cache: false,
					contentType: false,
					processData: false,
					success: function(msg){
							$("#thanks2").html('Image has been added.');
							$("#form-content").modal('hide');	
							var trHTML = '';
							var jsonData = JSON.parse(msg);
						if (typeof id == 'undefined'||id =='addid'){
							trHTML += '<tr id="tr'+ jsonData.newid[0] +'">';
							trHTML += '<td >' + jsonData.title  + '</td>';
							trHTML += '<td ><img src="'+ jsonData.thumnail +'"></td>';
							trHTML += '<td >'+ jsonData.uploaded_file +'</td>';
							trHTML += '<td >'+ jsonData.dates +'</td>';
							trHTML += '<td><a   href="#" class="confirm-edit btn mini red-stripe" role="button" data-title="'+ jsonData.title +'" data-id="'+ jsonData.newid[0] +'">Edit</a></td>';
							trHTML += '<td><a href="#" class="confirm-delete btn mini red-stripe" role="button" data-title="'+ jsonData.title +'" data-id="'+ jsonData.newid[0] +'">Delete</a></td>';
							trHTML += '</tr>';
							$('#ajax-box').append(trHTML);
						}else{
							trHTML += '<td >' + jsonData.title  + '</td>';
							trHTML += '<td ><img src="'+ jsonData.thumnail +'"></td>';
							trHTML += '<td >'+ jsonData.uploaded_file +'</td>';
							trHTML += '<td >'+ jsonData.dates +'</td>';
							trHTML += '<td><a   href="#" class="confirm-edit btn mini red-stripe" role="button" data-title="'+ jsonData.title +'" data-id="'+ jsonData.newid[0] +'">Edit</a></td>';
							trHTML += '<td><a href="#" class="confirm-delete btn mini red-stripe" role="button" data-title="'+ jsonData.title +'" data-id="'+ jsonData.newid[0] +' id=\'del\'">Delete</a></td>';
							$('#tr'+id).html(trHTML);
						}
					},
					error: function(){
						alert("Fail to upload new image, try again later.");
					}
				});
			});
			
		  });
				  
	</script>
		
</body>
</html>