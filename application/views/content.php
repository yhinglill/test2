
<div class="container">
	<div class="well well-large">
		<h2>Viewing All Uploads</h2>
		<div id="thanks2"><p></p></div>
		<div id="form-content" class="modal hide fade in" style="display: none;">
			<div class="modal-header">
				<a class="close" data-dismiss="modal">�</a>
				<h3>Upload New Image</h3>
			</div>
			<div class="modal-body">
				<form  class="contact" name="contact" id="data" method="post" enctype="multipart/form-data">
					<label class="btn btn-primary" for="my-file-selector">
					<input id="my-file-selector" name="avatar" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
					Browse
					</label>
					<span class='label label-info' id="upload-file-info"></span>
				</form>
			</div>
			<div class="modal-footer">
				<input class="btn btn-success" type="submit" value="Send!" id="submit">
				<a href="#" class="btn" data-dismiss="modal">Cancel</a>
			</div>
		</div>
		
		                   <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h3 id="myModalLabel">Delete</h3>
                        </div>
                        <div class="modal-body">
                            <p></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button data-dismiss="modal" class="btn red" id="btnYes">Confirm</button>
                        </div>
   </div><table class="table table-striped table-hover table-users">
    			<thead>
    				<tr>
    					
    					<th>Title</th>
    					<th>Thumbnail</th>
    					<th>Filename</th>
    					<th>Date added</th>
    					<th></th>
    					<th></th>
    				</tr>
    			</thead>

    			<tbody  id="ajax-box">
										
					<?php
					foreach ($posts as $post){
					?>
    				<tr id="tr<?php echo $post->id;?>">
                        
    					<td ><?php echo $post->title;?></td>
    					<td ><img src="<?php echo $post->thumnail;?>"></td>
    					<td ><?php echo $post->filename;?></td>
    					<td ><?php echo $post->date;?></td>
                    	  					
    					<td><a   href="#" class="confirm-edit btn mini red-stripe" role="button" data-title="<?php echo $post->title;?>" data-id="<?php echo $post->id;?>">Edit</a></td>

                        <td><a href="#" class="confirm-delete btn mini red-stripe" role="button" data-title="<?php echo $post->title;?>" data-id="<?php echo $post->id;?>">Delete</a></td>
                    </tr>
					<?php
					}
					?>
	               </tbody>

    		</table>
		</div>
</div>