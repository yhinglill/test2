<?php defined("SYSPATH") or die("No direct script access.");
error_reporting(E_ALL & ~E_DEPRECATED);
class Controller_Welcome extends Controller_Template {
    public $template = "template_front";
    
    public function before()
    {
        parent::before();
    }
	public function action_index()
	{
		$navigation = View::factory("navigation");
		$posts = ORM::factory('Welcome')->find_all();
		$content=View::factory('content')
			->bind('posts',$posts);
		$this->response->body($content);
		
		$this->template->navigation = $navigation->render();
		$this->template->content = $content->render();
	} 
	public function action_deletedata()
    {
		$entry_id = $this->request->param('id');
		DB::delete('welcomes')->where('id', '=', $entry_id)
                       ->execute(Database::instance());
    }
	public function action_getdata()
	{	
		if(!$this->request->is_ajax()){
			throw new HTTP_Exception_403;
		}else{
			$this->auto_render = false;
			$this->is_ajax = TRUE;
			header('content-type: application/json');
			$posts = ORM::factory('Welcome')->find_all();
			$content=View::factory('content')
				->bind('posts',$posts);
			foreach ($posts as $post ){
				$toj['title'][]		= $post->title;
				$toj['thumnail'][]	= $post->thumnail;
				$toj['filename'][]	= $post->filename;
				$toj['did'][]		= $post->id;
				$toj['date'][]		= $post->date;
				
			}
			$json = json_encode($toj);
			echo $json;
		}
	}
	public function action_uploadnew()
	{	
		if(!$this->request->is_ajax()){
			throw new HTTP_Exception_403;
		}else{
			
			$this->auto_render = false;
			$this->is_ajax = TRUE;			
			
			$content=View::factory('content');
			$error_message = NULL;
			$filename = NULL;
	 
			$entry_id = $this->request->param('id');
			
			if (isset($_FILES['file']))
			{
				$image = $_FILES['file'];
				$directory = DOCROOT.'uploads/';
 
				if ($file = Upload::save($image, NULL, $directory))
				{
				
					$filename = strtolower(Text::random('alnum', 20)).'.jpg';
					Image::factory($file)
						->resize(50, 50, Image::AUTO)
						->save($directory.'thumb_'.$filename);
					// Delete the temporary file
					unlink($file);
					$title = explode('.',$_FILES['file']['name']);
					$dates = date('Y-m-d H:i:s');
					// if edit entry
					if ($entry_id=='undefined'||$entry_id=='addid'){
						$newid = DB::insert('welcomes', array( 'title', 'thumnail', 'filename'))->values(array( $title[0], URL::base('http').'uploads/thumb_'.$filename, $filename))
						->execute(Database::instance());
					}else{
						DB::update('welcomes')->set(array('title' =>  $title[0] ,
															'thumnail' =>  URL::base('http').'uploads/thumb_'.$filename,
															'filename' =>  $filename,
															'date' => $dates
															))->where('id', '=', $entry_id)
						->execute(Database::instance());
						$newid = $entry_id;
					}
				}
			}
			
	 
			if ( ! $filename)
			{
				$error_message = 'There was a problem while uploading the image.
					Make sure it is uploaded and must be JPG/PNG/GIF file.';
			}
			$toj['uploaded_file'] = $filename;
			$toj['title'] = $title[0];
			$toj['thumnail'] = URL::base('http').'uploads/thumb_'.$filename;
			$toj['dates'] = $dates;
			$toj['newid'] = $newid;
			$toj['error_message'] = $error_message;
			$json = json_encode($toj);
			echo $json;
		}
	}
	
} // End Welcome


